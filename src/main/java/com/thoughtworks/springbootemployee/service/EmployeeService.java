package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.EmployeeIsInactiveException;
import com.thoughtworks.springbootemployee.exception.EmployeeNotFoundException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeInMemoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeInMemoryRepository employeeRepository;

    public EmployeeService(EmployeeInMemoryRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee findById(Long id) {
        Employee employee = employeeRepository.findById(id);
        if (employee == null) {
            throw new EmployeeNotFoundException();
        }
        return employee;
    }

    public List<Employee> list() {
        return employeeRepository.findAll();
    }

    public void create(Employee employee) {
        employee.setActive(true);
        employeeRepository.add(employee);
    }

    public void update(Employee employee) {
        Employee employeeToBeUpdated = employeeRepository.findById(employee.getId());
        if (!employeeToBeUpdated.isActive()) {
            throw new EmployeeIsInactiveException();
        }
        employeeToBeUpdated.setSalary(employee.getSalary());
        employeeToBeUpdated.setAge(employee.getAge());
        employeeRepository.update(employeeToBeUpdated);
    }

    public void delete(Long id) {
        Employee employeeToBeRemoved = employeeRepository.findById(id);
        if (employeeToBeRemoved == null) {
            throw new EmployeeNotFoundException();
        }
        employeeToBeRemoved.setActive(false);
        employeeRepository.update(employeeToBeRemoved);
    }
}
