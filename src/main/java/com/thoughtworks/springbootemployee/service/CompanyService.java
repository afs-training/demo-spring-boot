package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.CompanyNotFoundException;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.repository.CompanyInMemoryRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeInMemoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private final CompanyInMemoryRepository companyRepository;

    private final EmployeeInMemoryRepository employeeRepository;

    public CompanyService(CompanyInMemoryRepository companyRepository, EmployeeInMemoryRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    public Company findById(Long id) {
        Company company = companyRepository.findById(id);
        if (company == null) {
            throw new CompanyNotFoundException();
        }
        company.setEmployees(employeeRepository.findByCompanyId(id));
        return company;
    }

    public void add(Company company) {
        companyRepository.add(company);
    }

    public void update(Company company) {
        companyRepository.update(company);
    }

    public void delete(Long id) {
        companyRepository.delete(id);
        employeeRepository.deleteByCompanyId(id);
    }
}
