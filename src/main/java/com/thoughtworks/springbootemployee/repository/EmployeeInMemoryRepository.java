package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeInMemoryRepository {
    private static Map<Long, Employee> employees = new HashMap<>();
    private static AtomicLong atomicId = new AtomicLong(0);

    public void add(Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employees.put(employee.getId(), employee);
    }

    public Employee findById(Long id) {
        return employees.get(id);
    }


    public List<Employee> findAll() {
        return new ArrayList<>(employees.values());
    }


    public void delete(Long id) {
        employees.remove(id);
    }


    public void update(Employee employee) {
        Employee preEmployee = employees.get(employee.getId());
        preEmployee.setAge(employee.getAge());
        preEmployee.setSalary(employee.getSalary());
        preEmployee.setName(employee.getName());
        preEmployee.setGender(employee.getGender());
    }


    public void clear() {
        employees.clear();
    }

    public void deleteByCompanyId(Long companyId) {
        List<Employee> employeesToBeRemoved = employees.values().stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), companyId)).collect(Collectors.toList());
        employeesToBeRemoved.forEach(employeeToBeRemoved -> employees.remove(employeeToBeRemoved.getId()));
    }

    public List<Employee> findByCompanyId(Long companyId) {
        return employees.values().stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), companyId)).collect(Collectors.toList());
    }
}
