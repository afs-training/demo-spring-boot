package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class CompanyInMemoryRepository {
    private static final Map<Long, Company> companies = new HashMap<>();
    private static final AtomicLong atomicId = new AtomicLong(0);

    static {
        EmployeeInMemoryRepository inMemoryEmployeeRepository = new EmployeeInMemoryRepository();
        inMemoryEmployeeRepository.add(new Employee("alice", 19, "female", 6000.0));
        inMemoryEmployeeRepository.add(new Employee("bob", 20, "male", 6200.0));
        inMemoryEmployeeRepository.add(new Employee("charles", 21, "male", 5800.0));

        inMemoryEmployeeRepository.add(new Employee("ethan", 22, "male", 6000.0));
        inMemoryEmployeeRepository.add(new Employee("daisy", 23, "female", 6100.0));

        CompanyInMemoryRepository companyRepository = new CompanyInMemoryRepository();
        List<Employee> employees = inMemoryEmployeeRepository.findAll();
        int employeeSize = employees.size();
        companyRepository.add(new Company("spring", employees.subList(0, employeeSize - 2)));
        companyRepository.add(new Company("boot", employees.subList(employeeSize - 2, employeeSize)));
    }


    public void add(Company company) {
        company.setId(atomicId.incrementAndGet());
        companies.put(company.getId(), company);
    }


    public Company findById(Long id) {
        return companies.get(id);
    }


    public List<Company> findAll() {
        return new ArrayList<>(companies.values());
    }


    public void delete(Long id) {
        companies.remove(id);
    }


    public void update(Company company) {
        Company preCompany = companies.get(company.getId());
        if (preCompany == null) {
            return;
        }
        preCompany.setName(company.getName());
        preCompany.setEmployees(company.getEmployees());
    }


    public void clear() {
        companies.clear();
    }
}
