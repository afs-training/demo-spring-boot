package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeInMemoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc httpClient;

    private EmployeeInMemoryRepository employeeRepository = new EmployeeInMemoryRepository();

    @BeforeEach
    void tearDown() {
        employeeRepository.clear();
    }

    @Test
    void should_create_employee() throws Exception {
        String newEmployeeJson = "{\n" +
                "    \"name\": \"Lisa 10\",\n" +
                "    \"age\": 22,\n" +
                "    \"gender\": \"Female\",\n" +
                "    \"salary\": 9000\n" +
                "}";

        httpClient.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployeeJson))
                .andExpect(status().isCreated());
        List<Employee> employees = employeeRepository.findAll();
        Employee employee = employees.get(0);
        assertThat(employees).hasSize(1);
        assertThat(employee.getName()).isEqualTo("Lisa 10");
        assertThat(employee.isActive()).isTrue();
        assertThat(employee.getAge()).isEqualTo(22);
        assertThat(employee.getSalary()).isEqualTo(9000);
    }

    @Test
    void should_get_employees() throws Exception {
        employeeRepository.add(new Employee(1L, "sj", 30, "Male", 6666));
        httpClient.perform(MockMvcRequestBuilders.get("/employees")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].name").value("sj"))
                .andExpect(jsonPath("$[0].age").value(30))
                .andExpect(jsonPath("$[0].gender").value("Male"))
                .andExpect(jsonPath("$[0].salary").value(6666));
    }

    @Test
    void should_get_single_employee() throws Exception {
        employeeRepository.add(new Employee(1L, "sj", 30, "Male", 6666));
        Employee employee = new Employee(2L, "zy", 18, "Female", 8888);
        employeeRepository.add(employee);
        httpClient.perform(MockMvcRequestBuilders.get("/employees/{id}", employee.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.name").value("zy"))
                .andExpect(jsonPath("$.age").value(18))
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").value(8888));
    }

    @Test
    void should_update_employee() throws Exception {
        Employee employee = new Employee(10L, "sj", 25, "Male", 6666);
        employee.setActive(true);
        employeeRepository.add(employee);

        String newEmployeeJson = "{\n" +
                "    \"age\": 19,\n" +
                "    \"salary\": 8888\n" +
                "}";
        httpClient.perform(MockMvcRequestBuilders.put("/employees/{id}", employee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployeeJson))
                .andExpect(status().isOk());
        Employee updatedEmployee = employeeRepository.findById(employee.getId());
        assertThat(updatedEmployee.getId()).isEqualTo(employee.getId());
        assertThat(updatedEmployee.getName()).isEqualTo("sj");
        assertThat(updatedEmployee.getAge()).isEqualTo(19);
        assertThat(updatedEmployee.getSalary()).isEqualTo(8888);
        assertThat(updatedEmployee.getGender()).isEqualTo("Male");
    }
}
