package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyInMemoryRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeInMemoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    MockMvc httpClient;

    @Autowired
    CompanyInMemoryRepository companyRepository;

    @Autowired
    EmployeeInMemoryRepository employeeRepository;

    @BeforeEach
    void tearDown() {
        companyRepository.clear();
    }

    @Test
    void should_create_company() throws Exception {
        String newCompanyJson = "{\n" +
                "            \"name\": \"OOCL\"\n" +
                "        }";

        httpClient.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newCompanyJson))
                .andExpect(status().isCreated());
        List<Company> companies = companyRepository.findAll();
        Company company = companies.get(0);
        assertThat(companies).hasSize(1);
        assertThat(company.getName()).isEqualTo("OOCL");
    }

    @Test
    void should_find_companies() throws Exception {
        companyRepository.add(new Company("OOCL AFS"));
        httpClient.perform(MockMvcRequestBuilders.get("/companies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].name").value("OOCL AFS"));
    }

    @Test
    void should_find_single_company() throws Exception {
        Company ooclAfs = new Company("OOCL AFS");
        companyRepository.add(ooclAfs);

        Employee employee = new Employee("sj", 30, "Male", 6666);
        employee.setCompanyId(ooclAfs.getId());
        employeeRepository.add(employee);

        Company company = new Company("OOCL AFS2");
        companyRepository.add(company);

        httpClient.perform(MockMvcRequestBuilders.get("/companies/{id}", ooclAfs.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.name").value("OOCL AFS"))
                .andExpect(jsonPath("$.employees", hasSize(1)))
                .andExpect(jsonPath("$.employees[0].name").value("sj"))
                .andExpect(jsonPath("$.employees[0].age").value(30))
                .andExpect(jsonPath("$.employees[0].gender").value("Male"));
    }

    @Test
    void should_delete_company() throws Exception {
        Company company = new Company("OOCL AFS");
        companyRepository.add(company);

        Employee employee = new Employee("sj", 30, "Male", 6666);
        employee.setCompanyId(company.getId());
        employeeRepository.add(employee);

        httpClient.perform(MockMvcRequestBuilders.delete("/companies/{id}", company.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        Company deletedCompany = companyRepository.findById(company.getId());
        Employee deletedEmployee = employeeRepository.findById(employee.getId());
        assertThat(deletedCompany).isNull();
        assertThat(deletedEmployee).isNull();
    }

    @Test
    void should_throw_exception_given_company_not_found() throws Exception {
        Company company = new Company("OOCL AFS");
        companyRepository.add(company);
        httpClient.perform(MockMvcRequestBuilders.get("/companies/{id}", company.getId() + 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
