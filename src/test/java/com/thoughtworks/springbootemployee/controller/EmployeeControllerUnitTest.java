package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.exception.EmployeeIsInactiveException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class EmployeeControllerUnitTest {
    private EmployeeService employeeService = mock(EmployeeService.class);

    private EmployeeController employeeController = new EmployeeController(employeeService);

    private MockMvc httpClient = MockMvcBuilders.standaloneSetup(employeeController).build();


    @Test
    void should_create_employee() throws Exception {
        String newEmployeeJson = "{\n" +
                "    \"name\": \"Lisa 10\",\n" +
                "    \"age\": 22,\n" +
                "    \"gender\": \"Female\",\n" +
                "    \"salary\": 9000\n" +
                "}";

        httpClient.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployeeJson))
                .andExpect(status().isCreated());

        verify(employeeService).create(argThat(employee -> {
            assertEquals(employee.getName(), "Lisa 10");
            return true;
        }));
    }

    @Test
    void should_get_employees() throws Exception {
        given(employeeService.list()).willReturn(List.of(new Employee(1L, "sj", 30, "Male", 6666)));
        httpClient.perform(MockMvcRequestBuilders.get("/employees")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].name").value("sj"))
                .andExpect(jsonPath("$[0].age").value(30))
                .andExpect(jsonPath("$[0].gender").value("Male"))
                .andExpect(jsonPath("$[0].salary").value(6666));
    }

    @Test
    void should_get_single_employee() throws Exception {
        given(employeeService.findById(anyLong())).willReturn(new Employee(1L, "sj", 30, "Male", 6666));
        httpClient.perform(MockMvcRequestBuilders.get("/employees/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.name").value("sj"))
                .andExpect(jsonPath("$.age").value(30))
                .andExpect(jsonPath("$.gender").value("Male"))
                .andExpect(jsonPath("$.salary").value(6666));
        verify(employeeService, times(1)).findById(any());
    }

    @Test
    void should_update_employee() throws Exception {
        given(employeeService.findById(1L)).willReturn(new Employee(1L, "sj", 30, "Male", 6666));
        String newEmployeeJson = "{\n" +
                "    \"age\": 19,\n" +
                "    \"salary\": 8888\n" +
                "}";
        httpClient.perform(MockMvcRequestBuilders.put("/employees/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployeeJson))
                .andExpect(status().isOk());
        verify(employeeService, times(1)).update(argThat((employee) -> {
            assertEquals(employee.getId(), 1);
            assertEquals(employee.getAge(), 19);
            assertEquals(employee.getSalary(), 8888);
            return true;
        }));
    }

    @Test
    void should_return_bad_request_when_update_employee_given_not_active() throws Exception {
        // given
        Employee employee = new Employee(1, "Susan", 22, "Female", 7000);
        employee.setActive(false);

        willThrow(EmployeeIsInactiveException.class).given(employeeService).update(any());


        String newEmployeeJson = "{\n" +
                "    \"age\": 19,\n" +
                "    \"salary\": 8888\n" +
                "}";

        httpClient.perform(MockMvcRequestBuilders.put("/employees/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployeeJson))
                .andExpect(status().isBadRequest());
    }
}
