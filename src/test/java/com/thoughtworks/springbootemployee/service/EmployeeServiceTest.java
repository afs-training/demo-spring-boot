package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.EmployeeIsInactiveException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeInMemoryRepository;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class EmployeeServiceTest {

    private EmployeeInMemoryRepository employeeRepository = mock(EmployeeInMemoryRepository.class);

    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_set_inactive_when_delete_employee_given_active_employee() {
        // given
        Employee employee = new Employee(1, "Susan", 22, "Female", 7000);
        employee.setActive(true);

        given(employeeRepository.findById(anyLong())).willReturn(employee);

        // when
        employeeService.delete(1l);

        // then
        verify(employeeRepository).update(argThat((employee1) -> {
            assertThat(employee1.getName()).isEqualTo("Susan");
            assertThat(employee1.getId()).isEqualTo(1);
            assertFalse(employee1.isActive());
            return true;
        }));
    }

    @Test
    void should_throw_exception_when_update_employee_given_not_active() {
        // given
        Employee employee = new Employee(1, "Susan", 22, "Female", 7000);
        employee.setActive(false);

        given(employeeRepository.findById(any())).willReturn(employee);

        // when + then
        assertThrows(EmployeeIsInactiveException.class, () -> employeeService.update(employee));
    }
}